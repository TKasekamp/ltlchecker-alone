package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Util {
	/**
	 * Simple function to read in a file and returns a single string.
	 */
	public static String scanFile(String path) throws IOException {
		InputStreamReader reader = new InputStreamReader(new FileInputStream(new File(path)));
		BufferedReader scanner = new BufferedReader(reader);
		String answ = "";
		String line;
		while ((line = scanner.readLine()) != null) {
			if (!line.equals(""))
				answ += line + "\n";
		}
		scanner.close();

		return answ;
	}
}
