package test;

import static org.junit.Assert.assertEquals;

import java.util.Vector;

import org.deckfour.xes.model.XLog;
import org.junit.Before;
import org.junit.Test;
import org.processmining.plugins.ltlchecker.LTLChecker;
import org.processmining.plugins.ltlchecker.model.LTLModel;

/**
 * Trying to fix the parser.
 * 
 * @author TKasekamp
 *
 */
public class ParserTest {
	XLog log;

	@Before
	public void setup() {
		try {
			log = XLogReader.openLog("tests/testfiles/repairExample.xes");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void difficultFormulaTest() {
		LTLModel model = new LTLModel();
		model.setFile(difficultFormula());
		Vector rules = new Vector();
		rules.add("query");

		LTLChecker checker = new LTLChecker();
		checker.setSelectedRules(rules);
		Object[] result = checker.analyse(log, model);
		XLog comLog = (XLog) result[1];
		XLog ncomLog = (XLog) result[2];
		assertEquals("Matches everything", 1104, comLog.size());
		assertEquals(0, ncomLog.size());
	}
	
	@Test
	public void difficultFormulaTest2() {
		LTLModel model = new LTLModel();
		model.setFile(difficultFormula2());
		Vector rules = new Vector();
		rules.add("query");

		LTLChecker checker = new LTLChecker();
		checker.setSelectedRules(rules);
		Object[] result = checker.analyse(log, model);
		XLog comLog = (XLog) result[1];
		XLog ncomLog = (XLog) result[2];
		assertEquals("Matches everything", 1104, comLog.size());
		assertEquals(0, ncomLog.size());
	}
	
	@Test
	public void difficultFormulaTest3() {
		LTLModel model = new LTLModel();
		model.setFile(difficultFormula3());
		Vector rules = new Vector();
		rules.add("query");

		LTLChecker checker = new LTLChecker();
		checker.setSelectedRules(rules);
		Object[] result = checker.analyse(log, model);
		XLog comLog = (XLog) result[1];
		XLog ncomLog = (XLog) result[2];
		assertEquals("Matches everything", 1104, comLog.size());
		assertEquals(0, ncomLog.size());
	}
	
	@Test
	public void difficultFormulaTest4() {
		LTLModel model = new LTLModel();
		model.setFile(difficultFormula4());
		Vector rules = new Vector();
		rules.add("query");

		LTLChecker checker = new LTLChecker();
		checker.setSelectedRules(rules);
		Object[] result = checker.analyse(log, model);
		XLog comLog = (XLog) result[1];
		XLog ncomLog = (XLog) result[2];
		assertEquals("Matches everything", 1104, comLog.size());
		assertEquals(0, ncomLog.size());
	}
	
	@Test
	public void difficultFormulaTest5() {
		LTLModel model = new LTLModel();
		model.setFile(difficultFormula5());
		Vector rules = new Vector();
		rules.add("query");

		LTLChecker checker = new LTLChecker();
		checker.setSelectedRules(rules);
		Object[] result = checker.analyse(log, model);
		XLog comLog = (XLog) result[1];
		XLog ncomLog = (XLog) result[2];
		assertEquals("Matches everything", 1104, comLog.size());
		assertEquals(0, ncomLog.size());
	}

	private String difficultFormula() {
		// Double parentheses for unaryProp. Didn't work before
		String rule = "set ate.WorkflowModelElement;" + "rename ate.WorkflowModelElement as activity; \n"
				+ "formula query(A :activity, B: activity) := {} \n"
		+ "  <> (((( activity != B))));";
		return rule;
	}
	
	private String difficultFormula2() {
		// Single parentheses
		String rule = "set ate.WorkflowModelElement;" + "rename ate.WorkflowModelElement as activity; \n"
				+ "formula query(A :activity, B: activity) := {} \n"
		+ "  <> ( activity != B);";
		return rule;
	}
	
	private String difficultFormula3() {
		// THis is valid, must work
		String rule = "set ate.WorkflowModelElement;" + "rename ate.WorkflowModelElement as activity; \n"
				+ "formula query(A :activity, B: activity) := {} \n"
				+ " []( ( activity == A  -> <>( activity==B  ) ) );";

		return rule;
	}
	private String difficultFormula4() {
		// Normal formula, worked before
		String rule = "set ate.WorkflowModelElement;" + "rename ate.WorkflowModelElement as activity; \n"
				+ "formula query(A :activity, B: activity) := {} \n"
				+ " (( activity !=B _U activity == A) \\/ [] ( activity != B));";
		return rule;
	}
	private String difficultFormula5() {
		// 
		String rule = "set ate.WorkflowModelElement;" + "rename ate.WorkflowModelElement as activity; \n"
				+ "formula query(A :activity, B: activity) := {} \n"
				+ " ( ((activity !=B) _U (activity == A)) \\/ [] (((((( activity != B)))))));";
		return rule;
	}

}
