package org.processmining.plugins.ltlchecker;

import java.util.Vector;

public class CheckResultObject {
	private boolean[][] matrix;
	private InstanceModel[] instances;
	private RuleModel[] rules;
	private int badRules;
	private int badInstances;
	private Vector[][] failureType;
	private Vector[][] failurePlace;

	public Vector[][] getFailureType() {
		return failureType;
	}

	public void setFailureType(Vector[][] failureType) {
		this.failureType = failureType;
	}

	public Vector[][] getFailurePlace() {
		return failurePlace;
	}

	public void setFailurePlace(Vector[][] failurePlace) {
		this.failurePlace = failurePlace;
	}

	public int getBadRules() {
		return badRules;
	}

	public void setBadRules(int badRules) {
		this.badRules = badRules;
	}

	public int getBadInstances() {
		return badInstances;
	}

	public void setBadInstances(int badInstances) {
		this.badInstances = badInstances;
	}

	public boolean[][] getMatrix() {
		return matrix;
	}

	public void setMatrix(boolean[][] matrix) {
		this.matrix = matrix;
	}

	public InstanceModel[] getInstances() {
		return instances;
	}

	public void setInstances(InstanceModel[] instances) {
		this.instances = instances;
	}

	public RuleModel[] getRules() {
		return rules;
	}

	public void setRules(RuleModel[] rules) {
		this.rules = rules;
	}

}
