package org.processmining.plugins.ltlchecker.util;

import java.util.Arrays;

public class RemoveDouble {
	public static String[] eliminaDoppi(String vett[]) {
		if (vett.length < 2) {
			return vett;
		}
		String res[] = new String[vett.length];
		Arrays.sort(vett);

		int doppi = 0;
		res[0] = vett[0];
		int j = 1;
		for (int i = 1; i < vett.length; i++) {
			if (!vett[i].equals(vett[i - 1])) {
				res[j] = vett[i];
				++j;
			} else {
				++doppi;
			}
		}

		if (doppi == 0) {
			return res;
		}

		doppi = res.length - doppi;

		String res2[] = new String[doppi];
		for (int i = 0; i < doppi; i++) {
			res2[i] = res[i];
		}

		return res2;
	}
}
