package org.processmining.plugins.ltlchecker;

import org.deckfour.xes.model.XTrace;

public class CheckResultCell {
	private String rule;
	private String ltl;
	private boolean truthValue;
	private XTrace instance;

	public String getRule() {
		return rule;
	}

	public void setRule(String rule) {
		this.rule = rule;
	}

	public String getLtl() {
		return ltl;
	}

	public void setLtl(String ltl) {
		this.ltl = ltl;
	}

	public boolean isTruthValue() {
		return truthValue;
	}

	public void setTruthValue(boolean truthValue) {
		this.truthValue = truthValue;
	}

	public XTrace getInstance() {
		return instance;
	}

	public void setInstance(XTrace instance) {
		this.instance = instance;
	}

}
