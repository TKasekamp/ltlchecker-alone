package org.processmining.plugins.ltlchecker.model;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class LTLModel {

	String model;

	public void write(String path) {
		try {
			PrintStream ps = new PrintStream(new FileOutputStream(path));
			ps.println(model);
			ps.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getFile() {
		return model;
	}

	public void setFile(String file) {
		model = file;
	}

}
