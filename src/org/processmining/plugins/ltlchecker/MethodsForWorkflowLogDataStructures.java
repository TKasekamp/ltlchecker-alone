/***********************************************************
 * This software is part of the ProM package * http://www.processmining.org/ * *
 * Copyright (c) 2003-2006 TU/e Eindhoven * and is licensed under the * Common
 * Public License, Version 1.0 * by Eindhoven University of Technology *
 * Department of Information Systems * http://is.tm.tue.nl * *
 **********************************************************/

package org.processmining.plugins.ltlchecker;

import org.deckfour.xes.model.XAttributeLiteral;
import org.deckfour.xes.model.XTrace;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */

public class MethodsForWorkflowLogDataStructures {
	public static final String NUM_SIMILAR_INSTANCES = "numSimilarInstances";

	public static int getNumberSimilarProcessInstances(XTrace pi) {
		int numSimilarPIs = 1;
		if (pi.getAttributes().containsKey(NUM_SIMILAR_INSTANCES)) {
			try {
				if (((XAttributeLiteral) pi.getAttributes() != null)
						&& (((XAttributeLiteral) pi.getAttributes().get(NUM_SIMILAR_INSTANCES)) != null)) {
					numSimilarPIs = Integer
							.parseInt(((XAttributeLiteral) pi.getAttributes().get(NUM_SIMILAR_INSTANCES)).getValue());
				}
			} catch (NumberFormatException nfe) {
			}
		}
		return numSimilarPIs;
	}
}
