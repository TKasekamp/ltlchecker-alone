/***********************************************************
 * This software is part of the ProM package * http://www.processmining.org/ * *
 * Copyright (c) 2003-2006 TU/e Eindhoven * and is licensed under the * Common
 * Public License, Version 1.0 * by Eindhoven University of Technology *
 * Department of Information Systems * http://is.tm.tue.nl * *
 **********************************************************/

package org.processmining.plugins.ltlchecker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.info.XLogInfoFactory;
import org.deckfour.xes.model.XAttributeLiteral;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.plugins.ltlchecker.formulatree.FormulaNode;
import org.processmining.plugins.ltlchecker.formulatree.RootNode;
import org.processmining.plugins.ltlchecker.formulatree.ValueNode;
import org.processmining.plugins.ltlchecker.model.LTLModel;
import org.processmining.plugins.ltlchecker.parser.FormulaParameter;
import org.processmining.plugins.ltlchecker.parser.LTLParser;
import org.processmining.plugins.ltlchecker.parser.ParseException;
import org.processmining.plugins.ltlchecker.util.ParamData;
import org.processmining.plugins.ltlchecker.util.ParamTable;
import org.processmining.plugins.ltlchecker.util.SetsSet;
import org.processmining.plugins.ltlchecker.util.Substitutes;
import org.processmining.plugins.ltlchecker.util.TreeBuilder;

public class LTLChecker {

	/**
	 * A plugin for the ProM framework to check for properties specified in LTL
	 * on workflow logs of processes. It is an analysis plugin, so it can be
	 * used after mining has been done on the log. Needed for this plugin are a
	 * logReader object and a imported LTL Template file by the import plugin.
	 * 
	 * @version 0.2
	 * @author Fabrizio M. Maggi
	 */

	// FIELDS

	/** A table for valuate the parameters. */
	private Hashtable paramTable;
	// FORMER UI 
	private Vector selectedRules;
	private boolean firstSuccess;
	private boolean firstFailure;
	private boolean skipReady;

	/** Sets with the sets. */
	private SetsSet sets;

	//	private UIPluginContext context;
	private Vector[][] failureType;
	private Vector[][] failurePlace;
	private boolean[][] matrix;
	private RuleModel[] rules;
	private InstanceModel[] instances;

	public LTLChecker() {
		paramTable = new Hashtable();
		selectedRules = new Vector();
	}
	public Object[] analyse(XLog log, LTLModel model) {

		FileInputStream fis = null;
		LTLParser newparser = null;
		File fileLTL = null;

		try {
			fileLTL = File.createTempFile("tempLTL", ".ltl");
			PrintStream out = new PrintStream(fileLTL);
			out.println(model.getFile());
			fis = new FileInputStream(fileLTL.getAbsolutePath());
			newparser = new LTLParser(fis);
			newparser.setFilename(fileLTL.getAbsolutePath());
			newparser.init();
			newparser.parse();
			if (fis != null) {
				fis.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

		// Replaced GUI with this method
		Object[] settings = getSettings(newparser);

		return analyseHelper(log, newparser, settings);
	}

	public Object[] analyse(XLog log) {

		FileInputStream fis = null;
		LTLParser newparser = null;
		File fileLTL = null;

		try {

			InputStream ir = ClassLoader.getSystemClassLoader().getResourceAsStream("resources/standard.ltl");
			fileLTL = File.createTempFile("tempLTL", ".ltl");
			BufferedReader br = new BufferedReader(new InputStreamReader(ir));
			String line = br.readLine();

			PrintStream out = new PrintStream(fileLTL);
			while (line != null) {
				out.println(line);
				line = br.readLine();
			}
			out.flush();
			out.close();
			fis = new FileInputStream(fileLTL.getAbsolutePath());
			newparser = new LTLParser(fis);
			newparser.setFilename(fileLTL.getAbsolutePath());
			newparser.init();
			newparser.parse();
			if (fis != null) {
				fis.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		// Replaced GUI with this method
		Object[] settings = getSettings(newparser);

		return analyseHelper(log, newparser, settings);
	}

	/**
	 * Internal method for analyse. Created to decrease redundancy.
	 */
	private Object[] analyseHelper(XLog log, LTLParser newparser, Object[] settings) {
		Vector rules = (Vector) settings[0];
		paramTable = (Hashtable) settings[1];
		boolean firstSuccess = ((Boolean) settings[2]).booleanValue();
		boolean firstFailure = ((Boolean) settings[3]).booleanValue();
		boolean skipReady = ((Boolean) settings[4]).booleanValue();
		newparser = (LTLParser) settings[5];
		CheckResultObject outputFrame = declareMinerFrame(false, paramTable, log, newparser, rules, firstSuccess,
				firstFailure, skipReady);
		XLog comLog = (XLog) log.clone();
		XLog ncomLog = (XLog) log.clone();
		comLog.clear();
		ncomLog.clear();
		XConceptExtension.instance().assignName(comLog,
				"Compliant traces" + XConceptExtension.instance().extractName(comLog));
		XConceptExtension.instance().assignName(ncomLog,
				"Non Compliant traces" + XConceptExtension.instance().extractName(ncomLog));
		InstanceModel[] insts = outputFrame.getInstances();
		for (int h = 0; h < insts.length; h++) {
			XTrace newT = insts[h].getInstance();
			if (insts[h].isCorrect()) {
				comLog.add(newT);
			} else {
				ncomLog.add(newT);
			}
		}

		return new Object[] { outputFrame, comLog, ncomLog };
	}

	/**
	 * Copy from {@link CheckVisualizerGUI#getSettings()}
	 * 
	 * @param parser
	 * @return
	 */
	private Object[] getSettings(LTLParser parser) {
		Iterator i = parser.getVisibleFormulaNames().iterator();
		ParamTable selectParamTable = new ParamTable();

		while (i.hasNext()) {
			String formulaName = (String) i.next();
			ParamData pdCurr = createDataModel(parser.getParameters(formulaName));
			selectParamTable.setModel(pdCurr);
			Map<String, String> defaults = new HashMap<String, String>();
			for (Map.Entry<String, ValueNode> item : selectParamTable.getSubstitutes(parser).getAll().entrySet()) {
				defaults.put(item.getKey(), item.getValue().asParseableDefaultValue());
			}
			parser.setDefaultValues(formulaName, defaults);
			ParamData current = createDataModel(parser.getParameters(formulaName));
			selectParamTable.setModel(current);
			paramTable.put(formulaName, selectParamTable.getSubstitutes(parser));
		}
		Object[] out = new Object[6];
		out[0] = selectedRules;
		out[1] = paramTable;
		out[2] = firstSuccess;
		out[3] = firstFailure;
		out[4] = skipReady;
		out[5] = parser;
		return out;
	}

	public CheckResultObject declareMinerFrame(boolean console, Hashtable paramTable, XLog log, LTLParser parser,
			Vector enabledRules, boolean firstSuccess, boolean firstFailure, boolean skipReady) {
		Iterator logIt = log.iterator();
		int l = 0;
		int k = 0;
		XLogInfo info = XLogInfoFactory.createLogInfo(log);
		int numTraces = info.getNumberOfTraces();
		int numRules = enabledRules.size();
		instances = new InstanceModel[numTraces];
		XTrace temp = null;
		int i = 0;
		while (logIt.hasNext()) {
			temp = (XTrace) logIt.next();
			instances[i] = new InstanceModel();
			instances[i].setInstance(temp);
			instances[i].setCorrect(true);
			instances[i].setHealthDegree(0);
			i++;
		}
		int badRules = 0;
		int badInstances = 0;
		matrix = new boolean[numRules][numTraces];
		failureType = new Vector[numRules][numTraces];
		failurePlace = new Vector[numRules][numTraces];
		rules = new RuleModel[numRules];
		Substitutes sss = null;
		boolean run = true;
		k = 0;
		while ((k < numRules) && run) {
			//	System.out.println(k);
			if (!console) {
				//				prog.setValue(50 + (62 * k / enabledRules.size()));
			}
			String formulaName = (String) enabledRules.get(k);
			if (!((formulaName.equals("")) || (formulaName == null))) {
				int teller = 0;
				if (sets == null) {
					sets = new SetsSet(parser, log);
					sets.fill(log);
					//	System.out.println(sets);
					teller = numTraces;
				}
				TreeBuilder tb = new TreeBuilder(parser, formulaName, sets /*
																			 * ,
																			 * log
																			 * .
																			 * getLogSummary
																			 * (
																			 * )
																			 * .
																			 * getOntologies
																			 * (
																			 * )
																			 * null
																			 */);

				// Create the set of substitutes if any. THat is, if
				// it is an template, it can have already substitution
				// now, so add them. Otherwise, an empty substitutes
				// set is enough.

				RootNode root1 = new RootNode();
				sss = (Substitutes) paramTable.get(formulaName);
				sss.setBinder(root1);

				boolean fullfill = false;
				FormulaNode formula1 = (FormulaNode) tb.build(parser.getFormula(formulaName), sss, root1);
				root1.setFormula(formula1);

				rules[k] = new RuleModel();
				rules[k].setRuleName((String) enabledRules.get(k));
				rules[k].setLtlRule(formula1.toString());
				rules[k].setSatisfied(true);
				l = 0;
				while ((l < numTraces) && run) {
					RootNode root = new RootNode();
					sss = (Substitutes) paramTable.get(formulaName);
					sss.setBinder(root);
					FormulaNode formula = (FormulaNode) tb.build(parser.getFormula(formulaName), sss, root);
					root.setFormula(formula);
					failurePlace[k][l] = new Vector();
					failureType[k][l] = new Vector();

					XAttributeLiteral result = null;
					if (instances[l].getInstance().getAttributes() != null) {
						result = (XAttributeLiteral) instances[l].getInstance().getAttributes().get(formula.toString());
					}
					//CHECK
					if ((result != null) && skipReady) {
						fullfill = Boolean.valueOf(result.getValue()).booleanValue();
						matrix[k][l] = fullfill;
					}
					teller++;
					if ((result == null) || !skipReady) {
						// Because the pi must be walked through in reverse order, and the
						// framework does not support this, the ates of this pi are first
						// readed in into a list.

						LinkedList atesList = new LinkedList();
						Iterator ateIt = instances[l].getInstance().iterator();
						while (ateIt.hasNext()) {
							XEvent at = (XEvent) ateIt.next();
							atesList.add(at);
						}
						fullfill = false;
						for (int j = atesList.size(); j >= 0; j--) {
							// start with n + 1 ...
							fullfill = root.value(instances[l].getInstance(), atesList, j);
							RootNode currentRoot = new RootNode();
							FormulaNode currentFormula = (FormulaNode) tb.build(parser.getFormula(formulaName), sss,
									currentRoot);
							currentRoot.setFormula(currentFormula);
						}
						if (fullfill) {
							failurePlace[k][l] = new Vector();
							failureType[k][l] = new Vector();
						}
						matrix[k][l] = fullfill;
					}
					// The computed fullfill of the last ate of the pi is the value of
					// the whole pi, because by dynamic programming the other ates are
					// already computed and used.
					if (!fullfill && firstFailure) {
						// stop at first failure
						run = false;
					} else if (fullfill && firstSuccess) {
						// stop at first success
						run = false;
					}
					if (!fullfill) {
						if (instances[l].isCorrect()) {
							badInstances++;
						}
						if (rules[k].isSatisfied()) {
							badRules++;
						}
						instances[l].setCorrect(false);
						rules[k].setSatisfied(false);
					} else {
						double coverage = instances[l].getHealthDegree();
						coverage++;
						instances[l].setHealthDegree(coverage);
						coverage = rules[k].getCoverage();
						coverage++;
						rules[k].setCoverage(coverage);
					}
					l++;
				}
			}
			double coverage = rules[k].getCoverage();
			coverage = coverage / numTraces;
			if (l < numTraces) {
				coverage = -100;
			}
			rules[k].setCoverage(coverage);
			k++;
		}
		for (int m = 0; m < numTraces; m++) {
			double coverage = instances[m].getHealthDegree();
			coverage = coverage / numRules;
			if (k < numRules) {
				coverage = -100;
			}
			instances[m].setHealthDegree(coverage);
		}
		InstanceModel[] instMod = new InstanceModel[l];
		for (int j = 0; j < l; j++) {
			instMod[j] = instances[j];
		}
		RuleModel[] ruleMod = new RuleModel[k];
		for (int j = 0; j < k; j++) {
			ruleMod[j] = rules[j];
		}
		CheckResultObject crObj = new CheckResultObject();
		crObj.setInstances(instMod);
		crObj.setMatrix(matrix);
		crObj.setRules(ruleMod);
		crObj.setBadInstances(badInstances);
		crObj.setBadRules(badRules);
		crObj.setFailurePlace(failurePlace);
		crObj.setFailureType(failureType);
		//resultFr = new LTLVerificationResult(crObj);
		return crObj;
	}

	protected ParamData createDataModel(List<FormulaParameter> items) {
		return new ParamData(items);
	}

	protected ParamTable createParamTable() {
		return new ParamTable();
	}
	public Vector getSelectedRules() {
		return selectedRules;
	}
	public void setSelectedRules(Vector selectedRules) {
		this.selectedRules = selectedRules;
	}
	public boolean isFirstSuccess() {
		return firstSuccess;
	}
	public void setFirstSuccess(boolean firstSuccess) {
		this.firstSuccess = firstSuccess;
	}
	public boolean isFirstFailure() {
		return firstFailure;
	}
	public void setFirstFailure(boolean firstFailure) {
		this.firstFailure = firstFailure;
	}
	public boolean isSkipReady() {
		return skipReady;
	}
	public void setSkipReady(boolean skipReady) {
		this.skipReady = skipReady;
	}
}
