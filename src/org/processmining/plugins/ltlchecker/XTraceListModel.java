package org.processmining.plugins.ltlchecker;

import javax.swing.AbstractListModel;

import org.deckfour.xes.model.XTrace;

/**
 * @author Christian W. Guenther (christian@deckfour.org)
 * 
 */
public class XTraceListModel extends AbstractListModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4735900378220460350L;
	protected XTrace ateList;

	public XTraceListModel(XTrace list) {
		ateList = list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.ListModel#getElementAt(int)
	 */
	public Object getElementAt(int index) {
		try {
			return ateList.get(index);
		} catch (IndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.ListModel#getSize()
	 */
	public int getSize() {
		return ateList.size();
	}

}
