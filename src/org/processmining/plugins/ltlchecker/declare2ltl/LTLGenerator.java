package org.processmining.plugins.ltlchecker.declare2ltl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

import org.processmining.plugins.declareminer.visualizing.AssignmentModel;
import org.processmining.plugins.ltlchecker.model.LTLModel;

public class LTLGenerator {
	public static LTLModel generate(AssignmentModel model) {
		LTLModel result = new LTLModel();
		if (model.constraintDefinitionsCount() > 0) {
			String fileLTL = "tempLTL";
			try {
				File ltl = File.createTempFile(fileLTL, ".ltl");
				PrintStream out = new PrintStream(new File(ltl.getAbsolutePath()));
				ConstraintCollection collection = new ConstraintCollection(out);
				for (int i = 0; i < model.constraintDefinitionsCount(); i++) {
					collection.add(model.constraintDefinitionAt(i));
				}
				collection.write();

				String fileStr = "";
				BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(ltl)));
				String line = br.readLine();
				while (line != null) {
					fileStr = fileStr + line + "\n";
					line = br.readLine();
				}
				result.setFile(fileStr);
			} catch (FileNotFoundException ex) {
				ex.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	public static boolean generate(String file, AssignmentModel model) {
		boolean ok = false;
		if (model.constraintDefinitionsCount() > 0) {
			if (file != null) {
				try {
					PrintStream out = new PrintStream(new File(file));
					ConstraintCollection collection = new ConstraintCollection(out);
					for (int i = 0; i < model.constraintDefinitionsCount(); i++) {
						collection.add(model.constraintDefinitionAt(i));
					}
					collection.write();
					ok = true;
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
		}
		return ok;
	}
}
