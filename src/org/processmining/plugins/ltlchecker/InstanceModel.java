package org.processmining.plugins.ltlchecker;

import org.deckfour.xes.model.XTrace;

public class InstanceModel {
	XTrace instance;
	boolean correct;
	double healthDegree;

	public double getHealthDegree() {
		return healthDegree;
	}

	public void setHealthDegree(double healthDegree) {
		this.healthDegree = healthDegree;
	}

	public XTrace getInstance() {
		return instance;
	}

	public void setInstance(XTrace instance) {
		this.instance = instance;
	}

	public boolean isCorrect() {
		return correct;
	}

	public void setCorrect(boolean correct) {
		this.correct = correct;
	}
}
