/***********************************************************
 * This software is part of the ProM package * http://www.processmining.org/ * *
 * Copyright (c) 2003-2006 TU/e Eindhoven * and is licensed under the * Common
 * Public License, Version 1.0 * by Eindhoven University of Technology *
 * Department of Information Systems * http://is.tm.tue.nl * *
 **********************************************************/

package org.processmining.plugins.ltlchecker.formulatree;

import java.util.LinkedList;

import org.deckfour.xes.model.XTrace;
import org.processmining.plugins.ltlchecker.parser.AttributeNoValueException;
import org.processmining.plugins.ltlchecker.parser.DateAttribute;
import org.processmining.plugins.ltlchecker.parser.ParseAttributeException;

/**
 * DateCompNode is a node class of the formula tree denoting comparator
 * operators for dates: ==, !=, <=, >=, < and >.
 * 
 * @version 0.2
 * @author HT de Beer
 * @author Fabrizio M. Maggi
 */
public class DateCompNode extends CompNode {

	// FIELDS

	/** Lefthandside attribute of this comparison. */
	DateAttribute attribute;

	/**
	 * The righthandside expressiong of this comparison, that is in this case
	 * and StringValue.
	 */
	DateValueNode value;

	/**
	 * The operator of this node one of the constants defined in {@see CompNode}
	 * .*
	 */
	int op;

	// CONSTRUCTORS

	public DateCompNode(int op) {
		super();
		this.op = op;
	}

	// METHODS

	/**
	 * Set the attribute of this node.
	 * 
	 * @param attr
	 *            The attribute to set.
	 */
	public void setAttribute(DateAttribute attr) {
		attribute = attr;
	}

	/**
	 * Set the righthand side value of this node.
	 * 
	 * @param val
	 *            The value of this node.
	 */
	public void setValue(DateValueNode val) {
		value = val;
	}

	/**
	 * Compute the value of this node, that is te value of this node given the
	 * i-th ate of pi, computed by calling the value method of the children.
	 * 
	 * @param pi
	 *            The current process instance.
	 * @param ate
	 *            The current audit trail entry of pi.
	 * 
	 * @return The value of this node computed by calling the value method of
	 *         the children applied to the operator of this node.
	 */
	public boolean value(XTrace pi, LinkedList ates, int ateNr) {
		if (ateNr < ates.size()) {
			boolean result = false;
			try {
				nr = ateNr;

				attribute.value(pi, ates, ateNr);

				switch (op) {
					case CompNode.EQUAL : {
						if ((attribute != null) && (value != null)) {
							result = (attribute.value(pi, ates, ateNr).compareTo(value.value(pi, ates, ateNr)) == 0);
						}
					}
						;
						break;

					case CompNode.NOTEQUAL : {
						if ((attribute != null) && (value != null)) {
							result = (!(attribute.value(pi, ates, ateNr).compareTo(value.value(pi, ates, ateNr)) == 0));
						}
					}
						;
						break;

					case CompNode.LESSEREQUAL : {
						if ((attribute != null) && (value != null)) {
							result = (attribute.value(pi, ates, ateNr).compareTo(value.value(pi, ates, ateNr)) <= 0);
						}
					}
						;
						break;

					case CompNode.BIGGEREQUAL : {
						if ((attribute != null) && (value != null)) {
							result = (attribute.value(pi, ates, ateNr).compareTo(value.value(pi, ates, ateNr)) >= 0);
						}
					}
						;
						break;

					case CompNode.LESSER : {
						if ((attribute != null) && (value != null)) {
							result = (attribute.value(pi, ates, ateNr).compareTo(value.value(pi, ates, ateNr)) < 0);
						}
					}
						;
						break;

					case CompNode.BIGGER : {
						if ((attribute != null) && (value != null)) {
							result = (attribute.value(pi, ates, ateNr).compareTo(value.value(pi, ates, ateNr)) > 0);
						}
					}
						;
						break;
				}
				;

				return result;
			} catch (ParseAttributeException pae) {
				// Unable to parse the attribute or value, so return
				// false.
				//	Message.add(pae.getMessage(), Message.ERROR);
				return false;
			} catch (AttributeNoValueException anve) {
				// No value so false
				return false;
			}
		} else {
			// out of bounds ...
			return false;
		}
	}

	public String toString() {
		if ((attribute != null) && (value != null)) {
			return attribute.toString() + opAsString(op) + value.toString();
		} else {
			return "";
		}
	}
}
