/***********************************************************
 * This software is part of the ProM package * http://www.processmining.org/ * *
 * Copyright (c) 2003-2006 TU/e Eindhoven * and is licensed under the * Common
 * Public License, Version 1.0 * by Eindhoven University of Technology *
 * Department of Information Systems * http://is.tm.tue.nl * *
 **********************************************************/

package org.processmining.plugins.ltlchecker.formulatree;

import java.util.LinkedList;

import org.deckfour.xes.model.XTrace;

/**
 * EventuallyNode is a node class of the formula tree denoting the eventually
 * operator.
 * 
 * @version 0.2
 * @author HT de Beer
 * @author Fabrizio M. Maggi
 */
public class EventuallyNode extends UnaryNode {

	// FIELDS
	/**
	 * The next value, that is the previous computed value, needed to compute
	 * the value of the current (pi, ate) combination.
	 */
	private boolean next;
	private boolean nextInitialized;

	// CONSTRUCTORS

	public EventuallyNode() {
		super();
		// Added: initial value.
		nextInitialized = false;
	}

	// METHODS

	/**
	 * Compute the value of this node, that is te value of this node given the
	 * i-th ate of pi, computed by calling the value method of the child.
	 * 
	 * @param pi
	 *            The current process instance.
	 * @param ate
	 *            The current audit trail entry of pi.
	 * 
	 * @return The value of this node computed by calling the value method of
	 *         the child applied to the operator of this node.
	 */
	public boolean value(XTrace pi, LinkedList ates, int ateNr) {
		nr = ateNr;

		boolean result = false;
		if (ateNr >= ates.size()) {
			result = child.value(pi, ates, ateNr);
		} else {
			if (!nextInitialized) {
				// That is, next is not initialised, so do it now:
				next = child.value(pi, ates, ateNr);
				nextInitialized = true;
			}
			;
			boolean c = child.value(pi, ates, ateNr);
			result = (c || next);
		}
		;
		next = result;
		return result;
	}

	protected String getOperator() {
		return "<>";
	}
}
