/***********************************************************
 * This software is part of the ProM package * http://www.processmining.org/ * *
 * Copyright (c) 2003-2006 TU/e Eindhoven * and is licensed under the * Common
 * Public License, Version 1.0 * by Eindhoven University of Technology *
 * Department of Information Systems * http://is.tm.tue.nl * *
 **********************************************************/

package org.processmining.plugins.ltlchecker.formulatree;

import java.util.LinkedList;

import org.deckfour.xes.model.XTrace;

/**
 * UntilNode is a node class of the formula tree denoting the until operator.
 * 
 * @version 0.1
 * @author HT de Beer
 * @author Fabrizio M. Maggi
 */
public class UntilNode extends BinaryNode {

	// FIELDS
	/**
	 * The next value, that is the previous computed value, needed to compute
	 * the value of the current (pi, ate) combination.
	 */
	private boolean next;
	private boolean nextInitialized;

	// CONSTRUCTORS

	public UntilNode() {
		super();
		nextInitialized = false;
	}

	// METHODS

	/**
	 * Compute the value of this node, that is te value of this node given the
	 * i-th ate of pi, computed by calling the value method of the children.
	 * 
	 * @param pi
	 *            The current process instance.
	 * @param ate
	 *            The current audit trail entry of pi.
	 * 
	 * @return The value of this node computed by calling the value method of
	 *         the children applied to the operator of this node.
	 */
	public boolean value(XTrace pi, LinkedList ates, int ateNr) {
		nr = ateNr;
		boolean result = false;

		if (ateNr >= ates.size()) {
			// Empty list or a ate not in the list
			// Init value
			result = rightChild.value(pi, ates, ateNr);
		} else {
			if (!nextInitialized) {
				// That is, next is not initialised, so do it now:
				next = rightChild.value(pi, ates, ateNr);
				nextInitialized = true;
			}
			;
			result = (rightChild.value(pi, ates, ateNr) || (leftChild.value(pi, ates, ateNr) && next));
		}
		;
		next = result;
		return result;
	}

	protected String getOperator() {
		return "U_";
	}
}
