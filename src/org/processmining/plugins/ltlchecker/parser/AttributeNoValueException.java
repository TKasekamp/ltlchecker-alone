/***********************************************************
 * This software is part of the ProM package * http://www.processmining.org/ * *
 * Copyright (c) 2003-2006 TU/e Eindhoven * and is licensed under the * Common
 * Public License, Version 1.0 * by Eindhoven University of Technology *
 * Department of Information Systems * http://is.tm.tue.nl * *
 **********************************************************/

package org.processmining.plugins.ltlchecker.parser;

import org.deckfour.xes.model.XTrace;

/**
 * AttributeNoValueException is generated when a attribute does not exists in a
 * process instance or audit trail entry.
 * 
 * @version 0.1
 * @author HT de Beer
 */
public class AttributeNoValueException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5199839600494479309L;

	public AttributeNoValueException(XTrace pi, int ateNr, Attribute attr) {
		super("No element");
		//	"No element " + attr.toString() + " in pi" + pi.getName() +
		//	" - " + pi.getProcess() + " ( " + ateNr + " ).");
	}

}
