package org.processmining.plugins.ltlchecker.parser;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.deckfour.xes.model.XTrace;

public class ConceptSetAttribute extends SetAttribute {

	private final List<String> values;

	public ConceptSetAttribute(List<String> values, int kind, Attribute type) {
		super(toString(values), type.getScope(), Attribute.CONCEPTSET, kind);
		assert (kind == Attribute.LITERAL);
		assert (isConceptSet());
		this.values = values;
	}

	public String value(XTrace pi, LinkedList ates, int ateNr) throws AttributeNoValueException,
			ParseAttributeException {
		return ateNr < ates.size() ? getValue() : "";
	}

	public List<String> modelReferences(XTrace pi, LinkedList ates, int ateNr) throws AttributeNoValueException {
		return ateNr < ates.size() ? values : new ArrayList<String>(0);
	}

	public List<String> getModelReferences() {
		return values;
	}

	private static String toString(List<String> values) {
		String result = "";

		for (String uri : values) {
			result += " @" + uri;
		}
		return "[" + result + " ]";
	}
}
