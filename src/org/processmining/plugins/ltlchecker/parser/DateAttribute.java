/***********************************************************
 * This software is part of the ProM package * http://www.processmining.org/ * *
 * Copyright (c) 2003-2006 TU/e Eindhoven * and is licensed under the * Common
 * Public License, Version 1.0 * by Eindhoven University of Technology *
 * Department of Information Systems * http://is.tm.tue.nl * *
 **********************************************************/

package org.processmining.plugins.ltlchecker.parser;

import java.util.Date;
import java.util.LinkedList;
import java.util.Map;

import org.deckfour.xes.model.XAttributeLiteral;
import org.deckfour.xes.model.XAttributeTimestamp;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XTrace;

/**
 * DateAttribute is a specialised {@see Attribute} class implementing an value
 * method, used to compute the value of an attribute given the context.
 * 
 * @version 0.1
 * @author HT de Beer
 */
public class DateAttribute extends Attribute {

	public DateAttribute(String value, int scope, int type, int kind) {
		super(value, scope, type, kind);
	}

	public DateAttribute(String value, int kind, Attribute type) {
		super(value, kind, type);
	}

	/**
	 * The value method. Given an processinstance, a list of audit trail entries
	 * and the number of the `current' audit trail entry, give the value of this
	 * attribute as a number.
	 * 
	 * @param pi
	 *            The current process instance.
	 * @param ates
	 *            The list with audit trail entries of pi.
	 * @param ateNr
	 *            The number of the current ate in ates.
	 * 
	 * @return The value of this attribute given the context. That is, te value
	 *         of the literal or the value of the attribute of the current ate.
	 * 
	 * @throw AttributeNoValueException
	 * @throw ParseAttributeException
	 */
	public Date value(XTrace pi, LinkedList ates, int ateNr) throws AttributeNoValueException, ParseAttributeException {

		Date result = new Date();
		String stringValue = "";

		// For the emty list, the value is "". In principle, this value is
		// never computed because the formula results in some standard
		// value for the empty list.

		if (ateNr < ates.size()) {
			// If the list is not empty, the attribute can be `valuated'.

			switch (getKind()) {

				case Attribute.LITERAL : {
					// Just a constant, it should be parseble because it is
					// pared by the LTLParser, but nonetheless, it is tried.

					try {

						stringValue = getValue();
						result = sdf.parse(stringValue);

					} catch (java.text.ParseException e) {

						throw new ParseAttributeException(stringValue, this);

					}
					;
				}
					break;

				case Attribute.ATTRIBUTE : {
					// Get the value of this attribute given the context

					if (getScope() == Attribute.PI) {
						// Get the value out of pi

						Map data = pi.getAttributes();

						if (data.containsKey(getAttributeId().substring(3))) {

							try {

								stringValue = (String) data.get(getAttributeId().substring(3));
								result = sdf.parse(stringValue);

							} catch (java.text.ParseException e) {

								throw new ParseAttributeException(stringValue, this);

							}
							;

						} else {
							// No dataelement

							throw new AttributeNoValueException(pi, ateNr, this);
						}
						;
					} else {
						// scope is ATE, so get the value of the ate

						XEvent ate = (XEvent) ates.get(ateNr);
						String attrName = getAttributeId().substring(4);
						if (ate.getAttributes() != null) {
							if (attrName.equals("WorkflowModelElement")) {
								if (((XAttributeLiteral) ate.getAttributes().get("concept:name")) != null) {
									stringValue = ((XAttributeLiteral) ate.getAttributes().get("concept:name"))
											.getValue();
								}
							} else if (attrName.equals("EventType")) {
								if (((XAttributeLiteral) ate.getAttributes().get("lifecycle:transition")) != null) {
									stringValue = ((XAttributeLiteral) ate.getAttributes().get("lifecycle:transition"))
											.getValue();
								}
							} else if (attrName.equals("Originator")) {
								if (((XAttributeLiteral) ate.getAttributes().get("org:resource")) != null) {
									stringValue = ((XAttributeLiteral) ate.getAttributes().get("org:resource"))
											.getValue();
								}
							} else if (attrName.equals("Timestamp")) {
								if (((XAttributeTimestamp) ate.getAttributes().get("time:timestamp")) != null) {
									if (((XAttributeTimestamp) ate.getAttributes().get("time:timestamp")).getValue() == null) {
										throw new AttributeNoValueException(pi, ateNr, this);
									} else {
										result = ((XAttributeTimestamp) ate.getAttributes().get("time:timestamp"))
												.getValue();
									}
								}
							} else {
								// No standard element, get the data

								Map data = ate.getAttributes();

								if (data.containsKey(attrName)) {

									stringValue = ((XAttributeLiteral) ate.getAttributes().get(attrName)).getValue();

								} else {
									// No dataelement

									throw new AttributeNoValueException(pi, ateNr, this);
								}
								;
							}
							;
						}
						if (!attrName.equals("Timestamp")) {

							try {

								result = sdf.parse(stringValue);

							} catch (java.text.ParseException e) {

								throw new ParseAttributeException(stringValue, this);
							}
							;
						}
						;
					}
					;
				}
					;
					break;
			}
			;
		}
		;

		return result;
	}

}
