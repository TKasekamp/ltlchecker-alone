/***********************************************************
 * This software is part of the ProM package * http://www.processmining.org/ * *
 * Copyright (c) 2003-2006 TU/e Eindhoven * and is licensed under the * Common
 * Public License, Version 1.0 * by Eindhoven University of Technology *
 * Department of Information Systems * http://is.tm.tue.nl * *
 **********************************************************/

package org.processmining.plugins.ltlchecker.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.deckfour.xes.model.XAttributeLiteral;
import org.deckfour.xes.model.XAttributeTimestamp;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XTrace;

/**
 * SetAttribute is a specialised {@see Attribute} class implementing an value
 * method, used to compute the value of an attribute given the context.
 * 
 * @version 0.1
 * @author HT de Beer
 */
public class SetAttribute extends Attribute {

	private List<String> modelReferences = null;

	public SetAttribute(String value, int scope, int type, int kind) {
		super(value, scope, type, kind);
	}

	public SetAttribute(String value, int kind, Attribute type) {
		super(value, kind, type);
	}

	public SetAttribute(String value, int kind, Attribute type, List<String> modelReferences) {
		super(value, kind, type);
		this.modelReferences = modelReferences;
	}

	/**
	 * The value method. Given an processinstance, a list of audit trail entries
	 * and the number of the `current' audit trail entry, give the value of this
	 * attribute as a number.
	 * 
	 * @param pi
	 *            The current process instance.
	 * @param ates
	 *            The list with audit trail entries of pi.
	 * @param ateNr
	 *            The number of the current ate in ates.
	 * 
	 * @return The value of this attribute given the context. That is, te value
	 *         of the literal or the value of the attribute of the current ate.
	 * 
	 * @throw AttributeNoValueException
	 * @throw ParseAttributeException
	 */
	public String value(XTrace pi, LinkedList ates, int ateNr) throws AttributeNoValueException,
			ParseAttributeException {

		String result = "";
		String stringValue = "";

		// For the emty list, the value is "". In principle, this value is
		// never computed because the formula results in some standard
		// value for the empty list.

		if (ateNr < ates.size()) {
			// If the list is not empty, the attribute can be `valuated'.

			switch (getKind()) {

				case Attribute.LITERAL : {
					// Just a constant, it should be parseable because it is
					// pared by the LTLParser, but nonetheless, it is tried.
					stringValue = getValue();
					result = stringValue;
				}
					break;

				case Attribute.ATTRIBUTE : {
					// Get the value of this attribute given the context
					if (getScope() == Attribute.PI) {
						// Get the value out of pi
						Map data = null;
						if (pi.getAttributes() != null) {
							data = pi.getAttributes();
						} else {
							data = new HashMap();
						}

						if (data.containsKey(getAttributeId().substring(3))) {
							stringValue = (String) data.get(getAttributeId().substring(3));
							result = stringValue;
						} else {
							// No data element
							throw new AttributeNoValueException(pi, ateNr, this);
						}
					} else {
						// scope is ATE, so get the value of the ate
						XEvent ate = (XEvent) ates.get(ateNr);
						String attrName = getAttributeId().substring(4);
						if (ate.getAttributes() != null) {
							if (attrName.equals("WorkflowModelElement")) {
								if ((XAttributeLiteral) ate.getAttributes().get("concept:name") != null) {
									stringValue = ((XAttributeLiteral) ate.getAttributes().get("concept:name"))
											.getValue();
								}
							} else if (attrName.equals("EventType")) {
								if ((XAttributeLiteral) ate.getAttributes().get("lifecycle:transition") != null) {
									stringValue = ((XAttributeLiteral) ate.getAttributes().get("lifecycle:transition"))
											.getValue();
								}
							} else if (attrName.equals("Originator")) {
								if ((XAttributeLiteral) ate.getAttributes().get("org:resource") != null) {
									stringValue = ((XAttributeLiteral) ate.getAttributes().get("org:resource"))
											.getValue();
								}
							} else if (attrName.equals("Timestamp")) {
								if ((XAttributeTimestamp) ate.getAttributes().get("time:timestamp") != null) {
									if (((XAttributeTimestamp) ate.getAttributes().get("time:timestamp")).getValue() == null) {
										throw new AttributeNoValueException(pi, ateNr, this);
									} else {
										stringValue = ((XAttributeTimestamp) ate.getAttributes().get("time:timestamp"))
												.getValue().toString();
									}
								}
							} else {
								// No standard element, get the data
								Map data = ate.getAttributes();

								if (data.containsKey(attrName)) {
									stringValue = ((XAttributeLiteral) ate.getAttributes().get(attrName)).getValue()
											.toString();
								} else {
									// No dataelement
									throw new AttributeNoValueException(pi, ateNr, this);
								}
							}
							result = stringValue;
						}
					}
				}
					break;
			}
		}

		return result;
	}

	public List<String> modelReferences(XTrace pi, LinkedList ates, int ateNr) throws AttributeNoValueException {
		List<String> result = new ArrayList<String>(0);

		// For the emty list, the value is an empty list. In principle, this value is
		// never computed because the formula results in some standard
		// value for the empty list.

		/*
		 * if (ateNr < ates.size() && this.getKind() == ATTRIBUTE) { if
		 * (this.getScope() == Attribute.PI) { // Get the value out of pi String
		 * attrName = this.getAttributeId().substring(3); DataSection data = pi;
		 * 
		 * if (data.containsKey(attrName)) { result =
		 * data.getModelReferences(attrName); } else { // No data element throw
		 * new AttributeNoValueException(pi, ateNr, this); } } else { // scope
		 * is ATE, so get the value of the ate XEvent ate = (XEvent)
		 * ates.get(ateNr); String attrName =
		 * this.getAttributeId().substring(4);
		 * 
		 * if (attrName.equals("WorkflowModelElement")) { result = ate.g; } else
		 * if (attrName.equals("EventType")) { result =
		 * ate.getTypeModelReferences(); } else if
		 * (attrName.equals("Originator")) { result =
		 * ate.getOriginatorModelReferences(); } else if
		 * (attrName.equals("Timestamp")) { // time stamp doesn't have model
		 * references } else { // No standard element, get the data DataSection
		 * data = ate.getDataAttributes();
		 * 
		 * if (data.containsKey(attrName)) { result =
		 * data.getModelReferences(attrName); } else { // No data element throw
		 * new AttributeNoValueException(pi, ateNr, this); } } } } else if
		 * (ateNr < ates.size() && this.getKind() == LITERAL &&
		 * this.modelReferences != null) { result.addAll(this.modelReferences);
		 * }
		 */
		return result;
	}
}
