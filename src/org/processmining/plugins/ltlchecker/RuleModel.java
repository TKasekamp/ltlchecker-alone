package org.processmining.plugins.ltlchecker;

public class RuleModel {
	String ltlRule;
	String ruleName;
	double coverage;
	boolean satisfied;

	public double getCoverage() {
		return coverage;
	}

	public void setCoverage(double coverage) {
		this.coverage = coverage;
	}

	public boolean isSatisfied() {
		return satisfied;
	}

	public void setSatisfied(boolean satisfied) {
		this.satisfied = satisfied;
	}

	public String getLtlRule() {
		return ltlRule;
	}

	public void setLtlRule(String ltlRule) {
		this.ltlRule = ltlRule;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

}
