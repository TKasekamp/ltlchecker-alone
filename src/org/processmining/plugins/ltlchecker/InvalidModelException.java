package org.processmining.plugins.ltlchecker;

public class InvalidModelException extends Exception {

	private static final long serialVersionUID = 1884610777462523469L;

	public InvalidModelException(String msg) {
		super(msg);
	}

}
