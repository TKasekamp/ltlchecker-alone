# README #

This is a standalone version of LTLChecker with all the PRoM stuff removed. To use it, read the following. The important thing is that the ltl file has to be changed before it can be used by LTLChecker. 

The programm [LTLMiner](https://github.com/TKasekamp/LTLMiner) is referenced throughout this guide. 

1. Read in the ltl file with the formulas. Let's say the file consists of the formula 
```
#!java

    formula is_activity_of_first_state_A( A: activity ) :=
    {
    <h2>Is the activity of the first state equal to <b>A</b>?</h2>
    <p> Compare the activity of the first state with <b>A</b> </p>
    <p> Arguments:<br>
    <ul>
	<li><b>A</b> of type set (<i>ate.WorkflowModelElement</i>)</li>
    </ul>
    </p>
    }
      activity == A;
``` 
2. Generate all the possible combinations of formulas you want to test with default values. It might be possible to use the LTLChecker without default values (telling it directly what to check), but for me the default values were the easiest solution. So if you have an XLog with the activities named A, B, C, D and E and you want to check all those activities, the formulas would look like this. 

```
#!java

formula is_activity_of_first_state_A( A: activity :"A") :=
{
}
  activity == A;
  formula is_activity_of_first_state_A( A: activity :"B") :=
{
}
  activity == A;
  formula is_activity_of_first_state_A( A: activity :"C") :=
{
}
  activity == A;
  formula is_activity_of_first_state_A( A: activity :"D") :=
{
}
  activity == A;
  formula is_activity_of_first_state_A( A: activity :"E") :=
{
}
  activity == A;
```
3. Rename the formulas. This is because LTLChecker will not allow multiple formulas with the same name. In my program LTLMiner I just renamed them from "formula_0" to "formula_4":

```
#!java

formula formula_0( A: activity : "A") :=
{
}
  activity == A;
  formula formula_1( A: activity : "B") :=
{
}
  activity == A;
  formula formula_2( A: activity : "C") :=
{
}
  activity == A;
  formula formula_3( A: activity : "D") :=
{
}
  activity == A;
  formula formula_4( A: activity : "E") :=
{
}
  activity == A;
```
4. Create a valid LTLModel from all those formulas. Create a single String with all those formulas in it and add the necessary stuff to the front of the file, as seen [here](https://github.com/TKasekamp/LTLMiner/blob/master/src/main/java/ee/tkasekamp/ltlminer/LTLFileCreator.java). The LTLModel will just hold that String.

5. Create the LTLChecker object, tell it which formulas to check and analyse the XLog with the LTLModel. The two methods which do this are [here](https://github.com/TKasekamp/LTLMiner/blob/master/src/main/java/ee/tkasekamp/ltlminer/LTLMiner.java#L86-L103).

6. Interpret the output. The output is an Object[] and the checked formulas are in Object[0] as an CheckResultObject. See here on how to get the checked formulas. The RuleModel object contains the formula name, the formula itself and the coverage percentage. The Object[1] and Object[2] are XLog objects, but as I didn't use them, I can't really tell what they contain.  